const list = document.getElementById('list');
// const resetBtn = document.getElementById('resetBtn');

const url = 'https://rickandmortyapi.com/api/character';

fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;
        return rawData.map(character => {
          //all needed data is listed below as an entity 
            let { id, name, image, species, location, created, episode } = character;
            let allEpisodes = [];
            episode.forEach(e => {
                allEpisodes.push(` <a href=${e} target="blank">${e.substring(e.lastIndexOf('/')+1)}</a>`);
            });
            //create element
            const card = document.createElement('li');
            card.classList.add(`card-${id}`)
            card.innerHTML = `
            <div class="cards ">
                <h1>${name}</h1>
                <img src="${image}" alt="${name}">
                <p class="info species">Species: ${species}</p>
                <p class="info location">Location: <a href="${location.url}">${location.name}</a></p>
                <p class="info created">Created: ${created}</p>
                <p class="info episodes">Episodes: ${allEpisodes}</p>
            </div>`
            //append element
            list.appendChild(card);
        });
    })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });


function isVisible(elem) {
    let coords = elem.getBoundingClientRect();
    let windowHeight = document.documentElement.clientHeight;
    // верхний край элемента виден?
    // let topVisible = coords.top > 0 && coords.top < windowHeight;
    // нижний край элемента виден?
    let bottomVisible = coords.bottom < windowHeight && coords.bottom > 0;
  
    return  bottomVisible;
  }

function showAll() {
      if (isVisible(list)) {
        for(let i=10; i < 20; i++ ){
            let e = document.querySelector(`.card-${i}`)
            e.style.display = "list-item";
        }
      }
  }

// function resetShowAll() {
//     for(let i=10; i < 20; i++ ){
//         let e = document.querySelector(`.card-${i}`)
//         e.style.display = "none";
//     }
// }
  
showAll();
window.onscroll = showAll;


// resetBtn.addEventListener('click', evt => {
//     // console.log(evt.target.innerHTML);
//     resetShowAll()
// }, false)